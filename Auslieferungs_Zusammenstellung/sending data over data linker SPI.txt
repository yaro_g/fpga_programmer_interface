Protocol for sending data to the FPGA over the xmega SPI:

Data is always sent by bytes, i.e. if a data type consisting of more than one byte has to be sent,
the user has to take care to split it into bytes.

If repetition mode == one shot, 
	Inputs: 
		1) data
		2) whitespace followed by EOF
		
	The program waits for data at the standard input followed by an whitespace followed by EOF. After receiving whitespace followed by EOF, the data is sent.
	Received data from the FPGA will be output on standard output.
	Caution: EOF is not a real character. It is an internal command, that is only sent 
	when the pipe is closed (e.g. when the calling program terminates, (e.g. when a file is fully sent).
	This type of operation is intended to be used to send files.
	
If repetition mode == repetition,
	Inputs: 
		1) data link mode
		2) byte count
		3) data (only if data link mode != receive)
	
	First the program waits for the data link mode to be used.
	Secondly the program waits for the number of bytes to be sent (or received). Both numbers have to be sent to the 
	standard input as a decimal number in plain text format (e.g. 2314) followed by a whitespace character (e.g. space).
	Thirdly the program then waits for this amount of data (if data link mode != receive). 
	After receiving the data, data is sent (or received). The received data from the FPGA will be output to the standard output.
	This scheme will be repeated, until the number of bytes to be sent is zero. Then the program terminates.
	
If repetition mode == continuous
	Inputs: 
		1) data (separated by timeouts > 50ms)
		2) EOF
		
	The program waits for data at the standard input. The received data is sent immediately. 
	This mode is normally slower then "one shot" or "repetition".
	If EOF is received (e.g. because the sending program terminates), the program terminates.
	The received data from the FPGA will be output to the standard output.