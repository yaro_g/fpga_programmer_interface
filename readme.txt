Using the xmega to communicate with the FPGA:

Options:
-port <Name of the serial port (e.g. COM3)> //shortcut -p
	
	-program <datFile> //shortcut -prg
	programs the FPGA with the given .dat File. An erase is executed before programming.
	
	-verifiedProgram <datFile> //shortcut -vp
	programs the FPGA with the given .dat File. An erase is executed before programming. After programming a verification is executed
	
	-verify <datFile>  //shortcut -v
	verifies that the .dat file and the FPGA configuration match
	
	-info <datFile> //shortcut -i, datFile required for error checking
	prints the available info about the FPGA
	
	-version  //shortcut -vrs
	prints the PSHDL Board version
	
	-erase <datFile> //shortcut -e, datFile required for error checking
	erases the program on the FPGA
	

	-link spi | uart  //shortcut -l
	Data linker mode will be selected. PC can now communicate via the xmega	with the FPGA

		If spi selected: For sending data see: "sending data over data linker SPI.txt"
			-repMode oneShot | repetition | continous //shortcut -r   
			repetitionMode: continous is default
			
			-encIn bin | hex | dec //shortcut -ei 
			bin ist default, encoding of the input data
			
			-encOut bin | hex | dec //shortcut -eo
			bin ist default, encoding of the output data
			
			-sendOnly //shortcut -so
			Input channel is turned off (thus more bandwidth for output available)
			
			-readOnly //shortcut -ro
			Iutput channel is turned off (thus more bandwidth for input available)
		
		
		If uart selected: Everything incoming on stdin will be sent via uart. Everything received from xmega via uart will be put to stdout.
			-encIn bin | hex | dec //shortcut -ei 
			bin ist default, encoding of the input data
			-encOut bin | hex | dec //shortcut -eo
			bin ist default, encoding of the output data

			
Pin (button) forwarding:
	The Values of PA2 and PC3 are forwarded to PC5 and PC2 respectively.
	In SPI Data Linker mode this forwarding is disabled.
	Forwarding is synchronous to xmega Clock and has can have latency of several
	tenth of clock cycles to several thousands of clock cycles (if USB transfer ongoing).
	This latency can vary.
	Pin changes during this time may be lost or forwarded later.
	Pin changes of one Pin affect the forwarding latency of the other.
	This behavior can have a non deterministic effect on forwarding of bouncing.