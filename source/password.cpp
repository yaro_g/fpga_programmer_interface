/*
 * password.c
 *
 *  Created on: 03.05.2014
 *      Author: Yaro
 */

#include "password.h"

void sendUsbPassword(serial::Serial& my_serial){
	char password[] = USB_PASSWORD;

	my_serial.write((uint8_t*)password, strlen(password));
}

