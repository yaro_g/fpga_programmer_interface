/*
 * fileIO.cpp
 *
 *  Created on: 13.10.2013
 *      Author: Yaro
 */

#include "fileIO.h"


using namespace std;


void readFullFile(string fileName, vector<uint8_t>& data)
{
	ifstream myfile(fileName.c_str());
	if (!myfile.is_open()){
		throw string("Unable to open file");
	}

	myfile.seekg(0, std::ios::end);

	streamoff fileSize = myfile.tellg();

	//uint8_t *fileBuffer = new uint8_t[fileSize];
	data.resize(fileSize);

	myfile.seekg(0, std::ios::beg);
	myfile.read((char*)&data[0], fileSize);

}

