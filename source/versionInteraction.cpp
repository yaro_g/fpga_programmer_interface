/*
 * versionInteraction.cpp
 *
 *  Created on: 03.05.2014
 *      Author: Yaro
 */

#include "versionInteraction.h"

void handle_INFO_command(serial::Serial& my_serial){
	programmerMode nextProgrammerMode = Programmer_Mode_INFO;
	my_serial.write((uint8_t*) &nextProgrammerMode, 1);

	uint8_t majorVersion, minorVersion;
	my_serial.read(&majorVersion, 1);
	my_serial.read(&minorVersion, 1);

	std::cout << "PSHDL Board V" << (unsigned int)majorVersion << "." << (unsigned int)minorVersion << std::endl;
}

