/*
 * dataLinker.cpp
 *
 *  Created on: 18.11.2013
 *      Author: Yaro
 */

#include "dataLinkerInteraction.h"

using namespace std;

void writeDataLinkModeAndByteCount(serial::Serial& my_serial, dataLinkMode nextDataLinkMode, size_t byteCount) {
	vector<uint8_t> sendData;
	sendData.reserve(5);

	sendData.push_back((uint8_t) nextDataLinkMode);

	for (int j = 0; j < 4; j++) {
		uint8_t temp = byteCount >> j * 8;
		sendData.push_back(temp);
	}

	my_serial.write(&sendData[0], 5);
}

void sendAndPrintReceivedVectorSPI(serial::Serial& my_serial, vector<uint8_t>& sendData, dataLinkMode nextDataLinkMode, UserInteraction& outputUser) {
	size_t byteCount = sendData.size();
	//cout << "bytecount is " << sendData.size() << " \n";
	//cout.flush();

//	my_serial.write((uint8_t*) &nextDataLinkMode, 1);
//	writeUint32(my_serial, byteCount);

	writeDataLinkModeAndByteCount(my_serial, nextDataLinkMode, byteCount);

	//	cout << "\n received byte count " << readUint32(my_serial) << '\n'; //debug
	//	cout.flush();

	size_t bytesLeft = byteCount;
	while (bytesLeft != 0) {
		size_t processBytes;
		if (bytesLeft <= DATA_LINKER_PACKET_SIZE) {
			processBytes = bytesLeft;
		} else {
			processBytes = DATA_LINKER_PACKET_SIZE;
		}

		if (!(processBytes % 64)) { //bug workaround
			processBytes--;
		}

		uint8_t* sendBuf = &sendData[byteCount - bytesLeft];

		switch (nextDataLinkMode) {
		case Data_Link_Mode_RECEIVE: {
			vector<uint8_t> receiveBuf(processBytes);
			my_serial.read(&receiveBuf[0], processBytes);
			outputUser.outputData(receiveBuf);
		}
			break;
		case Data_Link_Mode_SEND: {
			my_serial.write(sendBuf, processBytes);
			break;
		}
		case Data_Link_Mode_SEND_AND_RECEIVE: {
			vector<uint8_t> receiveBuf(processBytes);
			my_serial.write(sendBuf, processBytes);
			my_serial.read(&receiveBuf[0], processBytes);
			outputUser.outputData(receiveBuf);
			break;
		}
		default:
			throw string("no such data link mode available");
		}

		bytesLeft -= processBytes;
	}

//	cout << "\nall sent!\n";
//	cout.flush();

	//debug //timing information handling
//	uint8_t temp = '\n';
//	while (temp != '\0') {
//		cout << (char) temp;
//		cout.flush();
//		my_serial.read(&temp, 1);
//	}
//	cout << "\nleft " << my_serial.available();
//	cout.flush();
}

void handle_USE_DATA_LINKER_SPI_command(serial::Serial& my_serial, UserInteraction& user) {
	programmerMode nextProgrammerMode = Programmer_Mode_DATA_LINKER_SPI;
	my_serial.write((uint8_t*) &nextProgrammerMode, 1);

	switch (user.getNextDataLinkerkRepetitionMode()) {
	case Data_Linker_Repetition_Mode_ONE_SHOT: {
		vector<uint8_t> sendData;

		dataLinkMode nextDataLinkMode = user.getSendData(sendData);
		sendAndPrintReceivedVectorSPI(my_serial, sendData, nextDataLinkMode, user);

		//leave data linker mode
		if (sendData.size() > 0) {
			sendData.clear();
			sendAndPrintReceivedVectorSPI(my_serial, sendData, Data_Link_Mode_RECEIVE, user);
		}
		break;
	}
	case Data_Linker_Repetition_Mode_REPETITIVE_WITH_BYTE_COUNT: {
		while (user.isInputAvailable() && !terminateFlag) {
			vector<uint8_t> sendData;
			dataLinkMode nextDataLinkMode = user.getSendData(sendData);

			sendAndPrintReceivedVectorSPI(my_serial, sendData, nextDataLinkMode, user);

		}
		break;
	}
	case Data_Linker_Repetition_Mode_CONTINUOUS: {
		pthread_t cinBufferdRead_Thread;
		ThreadSafeQueue<uint8_t> cinQueue;
		cinBufferdReadArguments cinBufferdReadArgs = { &cinQueue, user.getDataLinkInputDataFormat() };
		pthread_create(&cinBufferdRead_Thread, NULL, &cinBufferdRead, &cinBufferdReadArgs);

		vector<uint8_t> sendData;
		while (!cinQueue.isEofReached() && !terminateFlag) {
			sendData.clear();

			cinQueue.waitForNontrivialQueue();
			while (cinQueue.size() != 0) {
				sendData.push_back(cinQueue.remove());
			}

			sendAndPrintReceivedVectorSPI(my_serial, sendData, user.getNextDataLinkMode(), user);
		}

		//leave data linker mode
		if (sendData.size() > 0) {
			sendData.clear();
			sendAndPrintReceivedVectorSPI(my_serial, sendData, Data_Link_Mode_RECEIVE, user);
		}

		pthread_join(cinBufferdRead_Thread, NULL);
		break;
	}
	default:
		cerr << "Something wrong! Program should never reach this!";
	}
}

void sendVectorUART(serial::Serial& my_serial, vector<uint8_t>& sendData) {
	size_t byteCount = sendData.size();
	writeUint32(my_serial, (uint32_t)byteCount);

	size_t bytesLeft = byteCount;
	while (bytesLeft != 0) {
		size_t processBytes;
		if (bytesLeft <= DATA_LINKER_PACKET_SIZE) {
			processBytes = bytesLeft;
		} else {
			processBytes = DATA_LINKER_PACKET_SIZE;
		}

		if (!(processBytes % 64)) { //bug workaround
			processBytes--;
		}

		uint8_t* sendBuf = &sendData[byteCount - bytesLeft];

		my_serial.write(sendBuf, processBytes);

		bytesLeft -= processBytes;
	}
}

void handle_USE_DATA_LINKER_UART_command(serial::Serial& my_serial, UserInteraction& user) {
	pthread_mutex_t serialToOutputThread_finishFlag;
	initQuitMutexFlag(serialToOutputThread_finishFlag);

	pthread_t serialToOutput_Thread;
	struct outputSerialArguments outputSerial_args = { &my_serial, &serialToOutputThread_finishFlag, &user };
	pthread_create(&serialToOutput_Thread, NULL, outputSerial, &outputSerial_args);

	programmerMode nextProgrammerMode = Programmer_Mode_DATA_LINKER_UART;
	my_serial.write((uint8_t*) &nextProgrammerMode, 1);

	pthread_t cinBufferdRead_Thread;
	ThreadSafeQueue<uint8_t> cinQueue;
	cinBufferdReadArguments cinBufferdReadArgs = { &cinQueue, user.getDataLinkInputDataFormat() };
	pthread_create(&cinBufferdRead_Thread, NULL, &cinBufferdRead, &cinBufferdReadArgs);

	vector<uint8_t> sendData;
	while (!cinQueue.isEofReached()) {
		sendData.clear();

		cinQueue.waitForNontrivialQueue();
		while (cinQueue.size() != 0) {
			sendData.push_back(cinQueue.remove());
		}

		sendVectorUART(my_serial, sendData);
	}
	//leave data linker mode
	if (sendData.size() > 0) {
		sendData.clear();
		sendVectorUART(my_serial, sendData);
	}
	pthread_join(cinBufferdRead_Thread, NULL);

	tellThreadToQuit(serialToOutputThread_finishFlag);
	pthread_join(serialToOutput_Thread, NULL);
}
