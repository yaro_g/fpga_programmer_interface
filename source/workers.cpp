/*
 * workers.cpp
 *
 *  Created on: 23.11.2013
 *      Author: Yaro
 */

#include "workers.h"
#include <errno.h>

using namespace std;

void initQuitMutexFlag(pthread_mutex_t& mutexFinishFlag) {
	/* init and lock the mutex before creating the thread.  As long as the
	 mutex stays locked, the thread should keep running.  A pointer to the
	 mutex is passed as the argument to the thread function. */
	pthread_mutex_init(&mutexFinishFlag, NULL);
	pthread_mutex_lock(&mutexFinishFlag);
}

void tellThreadToQuit(pthread_mutex_t& mutexFinishFlag) {
	pthread_mutex_unlock(&mutexFinishFlag);
}

/* Returns 1 (true) if the mutex is unlocked, which is the
 * thread's signal to terminate.
 */
int needQuit(pthread_mutex_t *mtx) {
	switch (pthread_mutex_trylock(mtx)) {
	case 0: /* if we got the lock, unlock and return 1 (true) */
		pthread_mutex_unlock(mtx);
		return 1;
	case EBUSY: /* return 0 (false) if the mutex was locked */
		return 0;
	}
	return 1;
}

void* outputSerial(void *args) {
	outputSerialArguments &castedArgs = *((outputSerialArguments*) args);

	serial::Serial& my_serial = *(castedArgs.my_serial);
	pthread_mutex_t *mutexFinishFlag = castedArgs.mutexFinishFlag;
	UserInteraction &user = *(castedArgs.user);

	vector<uint8_t> buf;

	while (!needQuit(mutexFinishFlag)) {
		if (my_serial.available()) {
			buf.resize(my_serial.available());
			my_serial.read(&buf[0], buf.size());

			user.outputData(buf);
		} else {
			usleep(500);
		}
	}

	pthread_exit(NULL);
	return NULL;
}

void *cinBufferdRead(void *args) {
	cinBufferdReadArguments &castedArgs = *((cinBufferdReadArguments*) args);

	ThreadSafeQueue<uint8_t> &cinQueue = *(castedArgs.cinQueue);
	dataFormat userInputDataFormat = castedArgs.userInputDataFormat;

	uint32_t temp;

	switch (userInputDataFormat) {
	case Data_Format_BIN: {
		break;
	}
	case Data_Format_DEC: {
		cin >> dec;
		break;
	}
	case Data_Format_HEX: {
		cin >> hex;
		break;
	}
	default:
		throw string("Wrong data linker format (see readme.txt)");
	}

	if (userInputDataFormat == Data_Format_DEC || userInputDataFormat == Data_Format_HEX) {
		while (!cinQueue.isEofReached()) {
			cin >> temp;
			if (!cin.eof() && !terminateFlag) {
				cinQueue.add((uint8_t) temp);
			} else {
				cinQueue.setEofReached(1);
			}
		}
	} else if (userInputDataFormat == Data_Format_BIN) {
		char c;
		while (cin.get(c) && !terminateFlag) {
			cinQueue.add(c);
		}
		cinQueue.setEofReached(1);
	}

	pthread_exit(NULL);
	return NULL;
}

