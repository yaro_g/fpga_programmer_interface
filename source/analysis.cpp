/*
 * analysis.cpp
 *
 *  Created on: 01.11.2013
 *      Author: Yaro
 */

#include "analysis.h"

using namespace std;

void analyseData(vector<uint8_t> data){
	std::set<uint8_t> myset1;
	myset1.insert(data.begin(), data.end());

	std::cout << "containing elements:\n"; cout.flush();
	for (std::set<uint8_t>::iterator it = myset1.begin(); it != myset1.end(); ++it){
		size_t count = std::count (data.begin(), data.end(), *it);
		std::cout << (int)*it << " " << count << '\n' ;cout.flush();
	}

	std::set< std::vector<uint8_t> > myset2;
	std::multiset< std::vector<uint8_t> > mymultiset2;
	for (uint32_t i = 0; i < data.size()-1; i+=2){
		std::vector<uint8_t> temp(2); temp[0] = data[i]; temp[1] = data[i+1];
		myset2.insert(temp);
		mymultiset2.insert(temp);
	}

	std::cout << "containing double elements:\n";cout.flush();
	for (std::set< vector<uint8_t> >::iterator it = myset2.begin(); it != myset2.end(); ++it){
		size_t count = mymultiset2.count(*it);

		std::cout << (int)(*it)[0] << "_" <<(int)(*it)[1] << " " << count << '\n' ;cout.flush();
	}

	std::set< std::vector<uint8_t> > myset4;
	std::multiset< std::vector<uint8_t> > mymultiset4;
	for (uint32_t i = 0; i < data.size()-3; i+=4){
		std::vector<uint8_t> temp(4); temp[0] = data[i]; temp[1] = data[i+1]; temp[2] = data[i+2]; temp[3] = data[i+3];
		myset4.insert(temp);
		mymultiset4.insert(temp);
	}

	std::cout << "containing quadrupel elements:\n";cout.flush();
	for (std::set< vector<uint8_t> >::iterator it = myset4.begin(); it != myset4.end(); ++it){
		size_t count = mymultiset4.count(*it);

		std::cout << (int)(*it)[0] << "_" <<(int)(*it)[1] << "_" << (int)(*it)[2] << "_" <<(int)(*it)[3] << " " << count << '\n' ;cout.flush();
	}

}


