/*
 * programmerInteractions.cpp
 *
 *  Created on: 04.11.2013
 *      Author: Yaro
 */

#include "programmerInteractions.h"

using namespace std;

uint32_t readUint32(serial::Serial& my_serial) {
	uint32_t data = 0;
	uint8_t dataPart;

	for (int i = 0; i < 4; i++) {
		my_serial.read(&dataPart, 1);
		data |= (uint32_t) dataPart << i * 8;
	}

	return data;
}

uint16_t readUint16(serial::Serial& my_serial) {
	uint16_t data = 0;
	uint8_t dataPart;

	for (int i = 0; i < 2; i++) {
		my_serial.read(&dataPart, 1);
		data |= dataPart << i * 8;
	}

	return data;
}

void writeUint32(serial::Serial& my_serial, uint32_t data) {
	vector<uint8_t> sendData;
	sendData.reserve(4);

	for (int j = 0; j < 4; j++) {
		uint8_t temp = data >> j * 8;
		sendData.push_back(temp);
	}

	my_serial.write(&sendData[0], 4);
}

void writeUint16(serial::Serial& my_serial, uint16_t data) {
	vector<uint8_t> sendData;
	sendData.reserve(2);

	for (int j = 0; j < 2; j++) {
		uint8_t temp = data >> j * 8;
		sendData.push_back(temp);
	}

	my_serial.write(&sendData[0], 2);
}

void handle_PERFORM_ACTION_command(serial::Serial& my_serial, programmerAction nextProgrammerAction, uint8_t* fpgaImage,
		vector<uint8_t>& FPGAProgrammingSequence, vector<uint8_t>& compressedFPGAProgrammingSequence, vector<uint16_t>& rowDataCount) {

	programmerMode nextProgrammerMode = Programmer_Mode_FPGA_PROGRAMMER;
	my_serial.write((uint8_t*) &nextProgrammerMode, 1);

	my_serial.write((uint8_t*) &nextProgrammerAction, 1);

	programmerCommand nextProgrammerCommand = Programmer_Command_WAIT_FOR_COMMAND;

	while (nextProgrammerCommand != Programmer_Command_FINISH_CURRENT_ACTION) {
		uint8_t programmerInput;
		my_serial.read(&programmerInput, 1);
		nextProgrammerCommand = (programmerCommand) programmerInput;

		//cout<<"\ncommand received: "<<nextProgrammerCommand<<"\n"; cout.flush();

		switch (nextProgrammerCommand) {
		//				case Programmer_Command_SEND_DATA: { //sending data on demand
		//					uint32_t address = 0;
		//					uint8_t addressPart, byteCount;
		//
		//					for(int i = 0; i < 4; i++){
		//						my_serial.read(&addressPart, 1);
		//						address |= addressPart<<i*8;
		//					}
		//
		//					my_serial.read(&byteCount, 1);
		//
		//					my_serial.write(&fpgaImage[address], byteCount);
		//
		//					cout<<"\n asked for address "<<address<<"\n";cout.flush();
		//
		//					break;
		//				}
		case Programmer_Command_SEND_DATA: { //bulk data sending

//			cout << "\n asked ";
//			cout.flush();

			uint32_t byteCount = readUint32(my_serial);

//			cout << "for byteCount " << byteCount << "\n";
//			cout.flush();

			my_serial.write(fpgaImage, byteCount);

//			cout << "all sent" << "\n";
//			cout.flush();

			break;
		}

		case Programmer_Command_PRINT_STRING: {
			uint8_t byteCount;
			my_serial.read(&byteCount, 1);

			char* buf = new char[byteCount + 1];
			buf[byteCount] = '\0';

			my_serial.read((uint8_t*) buf, byteCount);

			cout << buf;
			cout.flush();

			delete[] buf;
			break;
		}

		case Programmer_Command_PRINT_STD_MESSAGE: {
			uint8_t messageNumber;
			my_serial.read(&messageNumber, 1);

			printStandardProgrammerMessage(messageNumber);
			break;
		}

		case Programmer_Command_GET_ROW_SIGNALS: { //currently not used
			cout << "\n";

			for (int i = 0; i < device_rows; i += 1) {
				my_serial.write(&FPGAProgrammingSequence[i * OUTPUT_BYTE_COUNT_PER_ROW], 1 * OUTPUT_BYTE_COUNT_PER_ROW);
				if (!(i % 100)) {
					cout << i << " of 2300 rows loaded\n";
					cout.flush();
				}
			}

			break;
		}

		case Programmer_Command_GET_ROW_SIGNALS_COMPRESSED: {
			cout << "\n";

			uint32_t sentBytes = 0;

			for (int i = 0; i < device_rows; i += 1) {

				writeUint16(my_serial, rowDataCount[i]);

				my_serial.write(&compressedFPGAProgrammingSequence[sentBytes], rowDataCount[i]);
				sentBytes += rowDataCount[i];

				if (!(i % 100)) {
					cout << i << " of 2300 rows loaded\n";
					cout.flush();
				}
			}

			break;
		}

		case Programmer_Command_FINISH_CURRENT_ACTION:
			cout << "\nCurrent action done!\n";
			cout.flush();
			break;

		default:
			cout << "wrong programmer command";
			cout.flush();
			break;
		}
	}
}

void printStandardProgrammerMessage(uint8_t messageNumber) {
	switch (messageNumber) {
	case 0:
		std::cout << "\r\nFPGA Array Encryption is enforced. Plain text programming and verification is prohibited.";
		std::cout.flush();
		break;
	case 1:
		std::cout
				<< "\r\nFPGA Array Encryption is not enforced.\r\nCannot gaurantee valid AES key present in target device.\r\nUnable to proceed with Encrypted FPGA Array operation.";
		std::cout.flush();
		break;
	case 2:
		std::cout << "\r\nFPGA Array Verification is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 3:
		std::cout << "\r\nFPGA Array Write/Erase is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
		std::cout << "\r\nFlashROM Encryption is enforced. Plain text programming and verification is prohibited.";
		std::cout.flush();
		break;
	case 4:
		std::cout
				<< "\r\nFlashROM Encryption is not enforced.\r\nCannot gaurantee valid AES key present in target device.\r\nUnable to proceed with Encrypted FlashROM programming.";
		std::cout.flush();
		break;
	case 5:
		std::cout << "\r\nFROM Verification is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 6:
		std::cout << "\r\nFROM Write/Erase is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 7:
		std::cout << "\r\nNVM block 0 Encryption is enforced. Plain text programming is prohibited.";
		std::cout.flush();
		break;
	case 8:
		std::cout
				<< "\r\nNVM block 0 Encryption is not enforced.\r\nCannot gaurantee valid AES key present in target device.\r\nUnable to proceed with Encrypted NVM programming.";
		std::cout.flush();
		break;
	case 9:
		std::cout << "\r\nNVM block 0 Read is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 10:
		std::cout << "\r\nNVM block 0 write is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 11:
		std::cout << "\r\nNVM block 1 Encryption is enforced. Plain text programming is prohibited.";
		std::cout.flush();
		break;
	case 12:
		std::cout
				<< "\r\nNVM block 1 Encryption is not enforced.\r\nCannot gaurantee valid AES key present in target device.\r\nUnable to proceed with Encrypted NVM programming.";
		std::cout.flush();
		break;
	case 13:
		std::cout << "\r\nNVM block 1 Read is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 14:
		std::cout << "\r\nNVM block 1 write is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 15:
		std::cout << "\r\nNVM block 2 Encryption is enforced. Plain text programming is prohibited.";
		std::cout.flush();
		break;
	case 16:
		std::cout
				<< "\r\nNVM block 2 Encryption is not enforced.\r\nCannot gaurantee valid AES key present in target device.\r\nUnable to proceed with Encrypted NVM programming.";
		std::cout.flush();
		break;
	case 17:
		std::cout << "\r\nNVM block 2 Read is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 18:
		std::cout << "\r\nNVM block 2 write is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 19:
		std::cout << "\r\nNVM block 3 Encryption is enforced. Plain text programming is prohibited.";
		std::cout.flush();
		break;
	case 20:
		std::cout
				<< "\r\nNVM block 3 Encryption is not enforced.\r\nCannot gaurantee valid AES key present in target device.\r\nUnable to proceed with Encrypted NVM programming.";
		std::cout.flush();
		break;
	case 21:
		std::cout << "\r\nNVM block 3 Read is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 22:
		std::cout << "\r\nNVM block 3 Write is protected by pass key.\r\nA valid pass key needs to be provided.";
		std::cout.flush();
		break;
	case 23:
		std::cout << "\r\nError, pass key match failure.";
		std::cout.flush();
		break;
	case 24:
		std::cout << "\r\nData file compiled for Dual Key device...";
		std::cout.flush();
		break;
	case 25:
		std::cout << "\r\nError: Failed to enable FPGA array.";
		std::cout.flush();
		break;
	case 26:
		std::cout << "\r\nFPGA array is not enabled.";
		std::cout.flush();
		break;
	case 27:
		std::cout << "\r\nFPGA array is programmed and enabled.";
		std::cout.flush();
		break;
	case 28:
		std::cout << "\r\nFailed to authenticate the encrypted data.\r\n";
		std::cout.flush();
		break;
	case 29:
		std::cout << "\r\nHardware interface is not selected.  Please assign hardware_interface to either IAP_SEL or GPIO_SEL prior to calling dp_top.";
		std::cout.flush();
		break;
	case 30:
		std::cout << "\r\nENABLE_GPIO_SUPPORT compile switch must be enabled to perform extrnal device programming.";
		std::cout.flush();
		break;
	case 31:
		std::cout << "\r\nENABLE_IAP_SUPPORT compile switch must be enabled to perform IAP programming.";
		std::cout.flush();
		break;
	case 32:
		std::cout
				<< "\r\nError: data file loaded is generated from Designer verion V8.5 or stand alone datgen utility. This version of the data file is not supported with ENABLE_CODE_SPACE_OPTIMIZATION compile switch enabled.  Disable ENABLE_CODE_SPACE_OPTIMIZATION compile switch in dpuser.h and try again.";
		std::cout.flush();
		break;
	case 33:
		std::cout << "\r\nFROM  erase is not supported with IAP on A2F200 devices. Refer to user guide for more information.";
		std::cout.flush();
		break;
	case 34:
		std::cout << "\r\nSecurity erase is not supported with IAP. Refer to user guide for more information.";
		std::cout.flush();
		break;
	case 35:
		std::cout << "\r\nFROM programming is not supported with IAP on A2F200 devices. Refer to user guide for more information.";
		std::cout.flush();
		break;
	case 36:
		std::cout << "\r\nSecurity programming is not supported with IAP. Refer to user guide for more information.";
		std::cout.flush();
		break;
	case 37:
		std::cout << "\r\nFROM programming is not supported with IAP on A2F200 devices. Refer to user guide for more information.";
		std::cout.flush();
		break;
	case 38:
		std::cout << "\r\nSecurity programming is not supported with IAP. Refer to user guide for more information.";
		std::cout.flush();
		break;
	case 39:
		std::cout << "\r\nUser Error: Action not found...";
		std::cout.flush();
		break;
	case 40:
		std::cout << "\r\nUser Error: Data file does not contain the data needed to support requested action.  Check original STAPL file configuration.";
		std::cout.flush();
		break;
	case 41:
		std::cout << "\r\nUser Error: Compiled code does not support the requesed action.  Re-compile the code with the arropriate compile option enabled...";
		std::cout.flush();
		break;
	case 42:
		std::cout
				<< "\r\nUser Error: IAP does not support FROM or security programming on A2F200 devices.  Regenerate data file without FROM and security support.";
		std::cout.flush();
		break;
	case 43:
		std::cout << "\r\nUser Error: IAP does not support security programming on A2F500.  Regenerate data file without security support.";
		std::cout.flush();
		break;
	case 44:
		std::cout << "\r\nUser Error: Compiled code does not support the requesed action.  Re-compile the code with the arropriate compile option enabled...";
		std::cout.flush();
		break;
	case 45:
		std::cout << "\r\nAction not found...";
		std::cout.flush();
		break;
	case 46:
		std::cout << "\r\nActID = ";
		std::cout.flush();
		break;
	case 47:
		std::cout << " ExpID = ";
		std::cout.flush();
		break;
	case 48:
		std::cout << "\r\nDevice Rev = ";
		std::cout.flush();
		break;
	case 49:
		std::cout << "\r\nSetting BSR Configurations...";
		std::cout.flush();
		break;
	case 50:
		std::cout << "\r\nLoading BSR...";
		std::cout.flush();
		break;
	case 51:
		std::cout << "\r\nInitializing Target Device...";
		std::cout.flush();
		break;
	case 52:
		std::cout << "\r\nFSN: ";
		std::cout.flush();
		break;
	case 53:
		std::cout << "\r\nVerifying device info...";
		std::cout.flush();
		break;
	case 54:
		std::cout << "\r\nUser LOCK Setting Error...Security";
		break;
	case 55:
		std::cout << "\r\nUROW Setting Error...Checksum";
		std::cout.flush();
		break;
	case 56:
		std::cout << "\r\nUROW Setting Error...Design";
		std::cout.flush();
		break;
	case 57:
		std::cout << "\r\nUROW Setting Error...";
		std::cout.flush();
		break;
	case 58:
		std::cout << "\r\nErase..."<<"\n#!>Erasing FPGA ";
		std::cout.flush();
		break;
	case 59:
		std::cout << "\r\nFailed Erase Operation";
		std::cout.flush();
		break;
	case 60:
		std::cout << "\r\nFailed UROW programming";
		std::cout.flush();
		break;
	case 61:
		std::cout << "\r\nExpected SILSIG: ";
		std::cout.flush();
		break;
	case 62:
		std::cout << "\r\nUser information: \r\nCYCLE COUNT: ";
		std::cout.flush();
		break;
	case 63:
		std::cout << "\r\nCHECKSUM = ";
		std::cout.flush();
		break;
	case 64:
		std::cout << "\r\nDesign Name = ";
		std::cout.flush();
		break;
	case 65:
		std::cout << "\r\nProgramming Method: ";
		std::cout.flush();
		break;
	case 66:
		std::cout << "IEEE1532";
		std::cout.flush();
		break;
	case 67:
		std::cout << "STAPL";
		std::cout.flush();
		break;
	case 68:
		std::cout << "DIRECTC";
		std::cout.flush();
		break;
	case 69:
		std::cout << "PDB";
		std::cout.flush();
		break;
	case 70:
		std::cout << "SVF";
		std::cout.flush();
		break;
	case 71:
		std::cout << "IAP";
		std::cout.flush();
		break;
	case 72:
		std::cout << "Unknown";
		std::cout.flush();
		break;
	case 73:
		std::cout << "\r\nAlgorithm Version: = ";
		std::cout.flush();
		break;
	case 74:
		std::cout << "\r\nProgrammer: = ";
		std::cout.flush();
		break;
	case 75:
		std::cout << "\r\nProgrammer: ";
		std::cout.flush();
		break;
	case 76:
		std::cout << "Flash Pro";
		std::cout.flush();
		break;
	case 77:
		std::cout << "Flash Pro Lite";
		std::cout.flush();
		break;
	case 78:
		std::cout << "FP3";
		std::cout.flush();
		break;
	case 79:
		std::cout << "Sculptor";
		std::cout.flush();
		break;
	case 80:
		std::cout << "BP Programmer";
		std::cout.flush();
		break;
	case 81:
		std::cout << "DirectC";
		std::cout.flush();
		break;
	case 82:
		std::cout << "STAPL";
		std::cout.flush();
		break;
	case 83:
		std::cout << "FP4";
		std::cout.flush();
		break;
	case 84:
		std::cout << "Unknown";
		std::cout.flush();
		break;
	case 85:
		std::cout << "\r\nSoftware Version = ";
		std::cout.flush();
		break;
	case 86:
		std::cout << "\r\n==================================================";
		std::cout.flush();
		break;
	case 87:
		std::cout << "\r\nSecurity Setting : ";
		std::cout.flush();
		break;
	case 88:
		std::cout << "\r\nFlashROM Write/Erase protected by pass key.";
		std::cout.flush();
		break;
	case 89:
		std::cout << "\r\nFlashROM Read protected by pass key.";
		std::cout.flush();
		break;
	case 90:
		std::cout << "\r\nArray Write/Erase protected by pass key.";
		std::cout.flush();
		break;
	case 91:
		std::cout << "\r\nArray Verify protected by pass key.";
		std::cout.flush();
		break;
	case 92:
		std::cout << "\r\nEncrypted FlashROM Programming Enabled.";
		std::cout.flush();
		break;
	case 93:
		std::cout << "\r\nEncrypted FPGA Array Programming Enabled.";
		std::cout.flush();
		break;
	case 94:
		std::cout << "\r\nNVM block 0 Write protected by pass key.";
		std::cout.flush();
		break;
	case 95:
		std::cout << "\r\nNVM block 0 Read protected by pass key.";
		std::cout.flush();
		break;
	case 96:
		std::cout << "\r\nNVM block 1 Write protected by pass key.";
		std::cout.flush();
		break;
	case 97:
		std::cout << "\r\nNVM block 1 Read protected by pass key.";
		std::cout.flush();
		break;
	case 98:
		std::cout << "\r\nEncrypted NVM block 0 Programming Enabled.";
		std::cout.flush();
		break;
	case 99:
		std::cout << "\r\nEncrypted NVM block 1 Programming Enabled.";
		std::cout.flush();
		break;
	case 100:
		std::cout << "\r\n==================================================";
		std::cout.flush();
		break;
	case 101:
		std::cout << "\r\nUSER_UROW = ";
		std::cout.flush();
		break;
	case 102:
		std::cout << "\r\nProgramming UROW...";
		std::cout.flush();
		break;
	case 103:
		std::cout
				<< "\r\nWarning: If you are programming a calibrated device, please regenerate the analog system configuration file with Libero 8.2 SP1 or greater.";
		std::cout.flush();
		break;
	case 104:
		std::cout << "\r\nErase FPGA Array...";
		std::cout.flush();
		break;
	case 105:
		std::cout << ".";
		std::cout.flush();
		break;
	case 106:
		std::cout << "\r\nPerforming Data Authentication...";
		std::cout.flush();
		break;
	case 107:
		std::cout << "\r\nProgramming Rlock...";
		std::cout.flush();
		break;
	case 108:
		std::cout << "\r\nVerifying FPGA Array..."<<"#!>Verifying FPGA";
		std::cout.flush();
		break;
	case 109:
		std::cout << "\r\nFailed to program FPGA Array at row ";
		std::cout.flush();
		break;
	case 110:
		std::cout << "\r\nProgramming FPGA Array..."<<"\n#!>Programming FPGA";
		std::cout.flush();
		break;
	case 111:
		std::cout << "\r\nFailed to program RLOCK ";
		std::cout.flush();
		break;
	case 112:
		std::cout << "\r\nProgramming RLOCK...";
		std::cout.flush();
		break;
	case 113:
		std::cout << "\r\nVerify 1 failed\r\nRow Number : ";
		std::cout.flush();
		break;
	case 114:
		std::cout << "\r\nVerify 0 failed\r\nRow Number : ";
		std::cout.flush();
		break;
	case 115:
		std::cout << "\r\n";
		std::cout.flush();
		break;
	case 116:
		std::cout << "\r\n\r\nFlashROM Information: ";
		std::cout.flush();
		break;
	case 117:
		std::cout << "\r\nProgramming FlashROM...";
		std::cout.flush();
		break;
	case 118:
		std::cout << "\r\nVerifying FlashROM...";
		std::cout.flush();
		break;
	case 119:
		std::cout << "\r\nErase FlashROM...";
		std::cout.flush();
		break;
	case 120:
		std::cout << "\r\nIdentifying device..."<<"\n#!>Reading .dat file";
		std::cout.flush();
		break;
	case 121:
		std::cout << "\r\nChecking data CRC...";
		std::cout.flush();
		break;
	case 122:
		std::cout << "\r\nExpected CRC=";
		std::cout.flush();
		break;
	case 123:
		std::cout << "\r\nData file is not valid. ";
		std::cout.flush();
		break;
	case 124:
		std::cout << "\r\nCRC verification failed.  Expected CRC = ";
		std::cout.flush();
		break;
	case 125:
		std::cout << " Actual CRC = ";
		std::cout.flush();
		break;
	case 126:
		std::cout << "\r\nData file is not loaded... \r\n";
		std::cout.flush();
		break;
	case 127:
		std::cout << "\r\nCalculating actual CRC...";
		std::cout.flush();
		break;
	case 128:
		std::cout << "\r\nReading Security...";
		std::cout.flush();
		break;
	case 129:
		std::cout << "\r\nProgramming SILSIG...";
		std::cout.flush();
		break;
	case 130:
		std::cout << "\r\nChecking for Dual Key Device...";
		std::cout.flush();
		break;
	case 131:
		std::cout << "\r\nError: Core enabled device detected...";
		std::cout.flush();
		break;
	case 132:
		std::cout << "\r\nFailed to verify AES Sec...";
		std::cout.flush();
		break;
	case 133:
		std::cout << "\r\nM7 Device Detected...";
		std::cout.flush();
		break;
	case 134:
		std::cout << "\r\nError: M7 Device Detected. Data file is not compiled for M7 Device...";
		std::cout.flush();
		break;
	case 135:
		std::cout << "\r\nError: M7 Device is not Detected. Data file is compiled for M7 Device...";
		std::cout.flush();
		break;
	case 136:
		std::cout << "\r\nFailed to verify AES Sec...";
		std::cout.flush();
		break;
	case 137:
		std::cout << "\r\nM1 Device Detected...";
		std::cout.flush();
		break;
	case 138:
		std::cout << "\r\nError: M1 Device Detected. Data file is not compiled for M1 Device...";
		std::cout.flush();
		break;
	case 139:
		std::cout << "\r\nError: M1 Device is not Detected. Data file is compiled for M1 Device...";
		std::cout.flush();
		break;
	case 140:
		std::cout << "\r\nFailed to verify AES Sec...";
		std::cout.flush();
		break;
	case 141:
		std::cout << "\r\nP1 Device Detected...";
		std::cout.flush();
		break;
	case 142:
		std::cout << "\r\nError: P1 Device Detected. Data file is not compiled for P1 Device...";
		std::cout.flush();
		break;
	case 143:
		std::cout << "\r\nError: P1 Device is not Detected. Data file is compiled for P1 Device...";
		std::cout.flush();
		break;
	case 144:
		std::cout << "\r\nFailed to verify AES Sec...";
		std::cout.flush();
		break;
	case 145:
		std::cout << "\r\nSingle Key Device Detected...";
		std::cout.flush();
		break;
	case 146:
		std::cout << "\r\nError: Data file is compiled for Dual Key Device...";
		std::cout.flush();
		break;
	case 147:
		std::cout << "\r\nDual Key Device Detected...";
		std::cout.flush();
		break;
	case 148:
		std::cout << "\r\nError: Data file is compiled for Single Key Device...";
		std::cout.flush();
		break;
	case 149:
		std::cout << "\r\nError: Page buffer size is not big enough...";
		std::cout.flush();
		break;

	case 255:
		std::cout << "\nunhandled error occured\n";
		std::cout.flush();
		break;
	default:
		std::cout << "\n unknown error message\n";
		std::cout.flush();
		break;
	}
}

