/*
 * compression.cpp
 *
 *  Created on: 01.11.2013
 *      Author: Yaro
 */

#include "compression.h"

using namespace std;

void getFPGAProgrammingSequenceSequenceOpcodes(vector<uint8_t>& FPGAProgrammingSequenceSequenceOpcodes, vector<uint8_t>& FPGAProgrammingSequenceSequence) {
	uint8_t opcode;

	for (uint32_t i = 0; i < FPGAProgrammingSequenceSequence.size(); i += 2) {
		switch (FPGAProgrammingSequenceSequence[i]) {
		case TRST:
			opcode = 0;
			break;
		case TRST | TMS:
			opcode = 1;
			break;
		case TRST | TDI:
			opcode = 2;
			break;
		case TRST | TDI | TMS:
			opcode = 3;
			break;

		default:
			cerr << "Problem! Strange data File\n";
			throw string("Problem! Strange data File\n");
		}

		//cout << "\n" << (int) FPGAProgrammingSequenceSequence[i] << " " << (int) opcode;

		FPGAProgrammingSequenceSequenceOpcodes.push_back(opcode);
	}
}

void compress(vector<uint8_t>& compressedFPGAProgrammingSequence, vector<uint16_t>& rowDataCount, vector<uint8_t>& FPGAProgrammingSequence) {
	vector<uint8_t> FPGAProgrammingSequenceSequenceOpcodes;
	FPGAProgrammingSequenceSequenceOpcodes.reserve(OUTPUT_BYTE_COUNT_PER_ROW / 2);
	getFPGAProgrammingSequenceSequenceOpcodes(FPGAProgrammingSequenceSequenceOpcodes, FPGAProgrammingSequence);


	for (uint32_t row = 0; row < device_rows; row++) {
		uint16_t currentRowDataCount = 0;

		for (uint32_t i = row * OUTPUT_BYTE_COUNT_PER_ROW / 2; i < (row + 1) * OUTPUT_BYTE_COUNT_PER_ROW / 2; i++) {

			uint8_t currentOpcode = FPGAProgrammingSequenceSequenceOpcodes[i];

			uint8_t opcodeCount = 1;

			while (i + 1 < (row + 1) * OUTPUT_BYTE_COUNT_PER_ROW / 2 && FPGAProgrammingSequenceSequenceOpcodes[i + 1] == currentOpcode && opcodeCount < 64) {
				opcodeCount++;
				i++;
			}

			compressedFPGAProgrammingSequence.push_back(currentOpcode | (opcodeCount << 2));

			currentRowDataCount++;
		}
		rowDataCount.push_back(currentRowDataCount);
	}
}

void decompressDebug(vector<uint8_t>& compressedFPGAProgrammingSequence, vector<uint8_t>& FPGAProgrammingSequence) {
	vector<uint8_t> rowData;
	vector<uint8_t> wholeSequence;

	uint8_t outData, outDataTCK;
	uint8_t received;
	uint32_t receiveCount = 0;

	for (uint16_t row = 0; row < device_rows; row++) {
		vector<uint8_t>().swap(rowData);
		rowData.reserve(2398);

		for (uint32_t i = row * OUTPUT_BYTE_COUNT_PER_ROW / 2; i < (uint32_t) ((row + 1) * OUTPUT_BYTE_COUNT_PER_ROW / 2);) {
			if (receiveCount >= compressedFPGAProgrammingSequence.size()) {
				cerr << "want too much!";
				cerr.flush();
			}

			received = compressedFPGAProgrammingSequence[receiveCount++];

			switch (received & 0x03) {
			case 0:
				outData = TRST;
				break;
			case 1:
				outData = TRST | TMS;
				break;
			case 2:
				outData = TRST | TDI;
				break;
			case 3:
				outData = TRST | TDI | TMS;
				break;
			}

			outDataTCK = outData | TCK;

			uint8_t repeatCount = received >> 2;

			i += repeatCount;
			for (; repeatCount > 0; repeatCount--) {
				rowData.push_back(outData);
				rowData.push_back(outDataTCK);

				wholeSequence.push_back(outData);
				wholeSequence.push_back(outDataTCK);
			}

		}

		if (!std::equal(rowData.begin(), rowData.end(), FPGAProgrammingSequence.begin() + row * OUTPUT_BYTE_COUNT_PER_ROW)) {
			cerr << "\nerror while decoding! row " << row;
			cerr.flush();
		}

	}

	if (!std::equal(wholeSequence.begin(), wholeSequence.end(), FPGAProgrammingSequence.begin())) {
		cerr << "\nerror while decoding all!";
		cerr.flush();
	}

}

