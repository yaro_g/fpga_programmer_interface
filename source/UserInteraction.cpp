/*
 * user.cpp
 *
 *  Created on: 06.11.2013
 *      Author: Yaro
 */

#include "UserInteraction.h"

using namespace std;


UserInteraction::UserInteraction(int argc, char *argv[]) {

	datFilePath = NULL;
	nextProgrammerAction = DP_NO_ACTION_FOUND;
	nextDataLinkerRepetitionMode = Data_Linker_Repetition_Mode_CONTINUOUS;
	dataLinkInputDataFormat = Data_Format_BIN;
	dataLinkOutputDataFormat = Data_Format_BIN;
	nextDataLinkMode = Data_Link_Mode_SEND_AND_RECEIVE;
	nextOperationMode = Operation_Mode_NO_COMMAND;

	pthread_mutex_init(&coutSyncMutex, NULL);

	if (argc < 4) {
		throw string("not enough input arguments (see readme.txt)");
	}


	static struct option argumentOptions[] = {
				{ "port", required_argument, 0, 'p' }, 				{ "p", required_argument, 0, 'p' },

				{ "program",		 required_argument, 0, 'm' }, 	{ "prg", required_argument, 0, 'm' },
				{ "verifiedProgram", required_argument, 0, 'v' }, 	{ "vp", required_argument, 0, 'v' },
				{ "verify",			 required_argument, 0, 'y' }, 	{ "v",	required_argument, 0, 'y' },
				{ "info", 			 required_argument, 0, 'i' },	{ "i", required_argument, 0, 'i' },
				{ "erase", 			 required_argument, 0, 'e' }, 	{ "e", required_argument, 0, 'e' },
				{ "version", 		 no_argument, 0, 'o' }, 		{ "vrs", no_argument, 0, 'o' },

				{ "link",	required_argument, 0, 'l' }, 			{ "l",	required_argument, 0, 'l' },
				{ "repMode",required_argument, 0, 'r' }, 			{ "r",	required_argument, 0, 'r' },
				{ "encIn",	required_argument, 0, 'n' },		 	{ "ei",	required_argument, 0, 'n' },
				{ "encOut",	required_argument, 0, 'u' }, 			{ "eo",	required_argument, 0, 'u' },

				{ "sendOnly", no_argument, 0, 's'}, 				{ "so",	no_argument, 0, 's' },
				{ "readOnly", no_argument, 0, 'd'}, 				{ "ro",	no_argument, 0, 'd'},

				{ "debug", no_argument, 0, 'g'}, 					{ "dbg",	no_argument, 0, 'g'},

				{0,0,0,0}
		};

		int opt;
		while ((opt = getopt_long_only(argc, argv,"", argumentOptions, NULL )) != -1) {
			switch (opt) {
				case 'p' :
					serialPortName = optarg;
					break;
				case 'm' :
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					nextOperationMode = Operation_Mode_PERFORM_PROGRAMMER_ACTION;
					nextProgrammerAction = DP_PROGRAM_WITHOUT_VERIFY_ACTION_CODE;
					datFilePath = optarg;
					break;
				case 'v' :
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					nextOperationMode = Operation_Mode_PERFORM_PROGRAMMER_ACTION;
					nextProgrammerAction = DP_PROGRAM_ACTION_CODE;
					datFilePath = optarg;
					break;
				case 'y' :
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					nextOperationMode = Operation_Mode_PERFORM_PROGRAMMER_ACTION;
					nextProgrammerAction = DP_VERIFY_ACTION_CODE;
					datFilePath = optarg;
					break;
				case 'i' :
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					nextOperationMode = Operation_Mode_PERFORM_PROGRAMMER_ACTION;
					nextProgrammerAction = DP_DEVICE_INFO_ACTION_CODE;
					datFilePath = optarg;
				 	break;
				case 'e' :
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					nextOperationMode = Operation_Mode_PERFORM_PROGRAMMER_ACTION;
					nextProgrammerAction = DP_ERASE_ACTION_CODE;
					datFilePath = optarg;
					break;
				case 'o':
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					nextOperationMode = Operation_Mode_INFO;
					break;
				case 'l':
					if(nextOperationMode != Operation_Mode_NO_COMMAND){
						throw string("Options program, verifiedProgram, verify, info, erase, info and link are mutually exclusive");
					}
					if(strcmp(optarg,"spi") == 0){
						nextOperationMode = Operation_Mode_USE_DATA_LINKER_SPI;
				 	}else if(strcmp(optarg,"uart") == 0){
				 		nextOperationMode = Operation_Mode_USE_DATA_LINKER_UART;
				 	}else{
						stringstream ss;
						ss<<"wrong argument to option \"link\": \n\tactual: "<<optarg<<"\n\texpected: spi | uart";
						throw ss.str();
				 	}
				break;
				case 'r':
					if(strcmp(optarg,"oneShot") == 0){
						nextDataLinkerRepetitionMode = Data_Linker_Repetition_Mode_ONE_SHOT;
					}else if(strcmp(optarg,"repetition") == 0){
						nextDataLinkerRepetitionMode = Data_Linker_Repetition_Mode_REPETITIVE_WITH_BYTE_COUNT;
					}else if(strcmp(optarg,"continous") == 0){
						nextDataLinkerRepetitionMode = Data_Linker_Repetition_Mode_CONTINUOUS;
					}else{
						stringstream ss;
						ss<<"wrong argument to option \"repMode\": \n\tactual: "<<optarg<<"\n\texpected: oneShot | repetition | continous";
						throw ss.str();
					}
					break;
				case 'n':
					if(strcmp(optarg,"bin") == 0){
						dataLinkInputDataFormat = Data_Format_BIN;
					}else if(strcmp(optarg,"hex") == 0){
						dataLinkInputDataFormat = Data_Format_HEX;
					}else if(strcmp(optarg,"dec") == 0){
						dataLinkInputDataFormat = Data_Format_DEC;
					}else{
						stringstream ss;
						ss<<"wrong argument to option \"encIn\": \n\tactual: "<<optarg<<"\n\texpected: bin | hex | dec";
						throw ss.str();
					}
					break;
				case 'u':
					if(strcmp(optarg,"bin") == 0){
						dataLinkOutputDataFormat = Data_Format_BIN;
					}else if(strcmp(optarg,"hex") == 0){
						dataLinkOutputDataFormat = Data_Format_HEX;
					}else if(strcmp(optarg,"dec") == 0){
						dataLinkOutputDataFormat = Data_Format_DEC;
					}else{
						stringstream ss;
						ss<<"wrong argument to option \"encOut\": \n\tactual: "<<optarg<<"\n\texpected: bin | hex | dec";
						throw ss.str();
					}
					break;
				case 's' :
					if(nextDataLinkMode != Data_Link_Mode_SEND_AND_RECEIVE){
						throw string("Options sendOnly and readOnly are mutually exclusive");
					}
					nextDataLinkMode = Data_Link_Mode_SEND;
					break;
				case 'd' :
					if(nextDataLinkMode != Data_Link_Mode_SEND_AND_RECEIVE){
						throw string("Options sendOnly and readOnly are mutually exclusive");
					}
					nextDataLinkMode = Data_Link_Mode_RECEIVE;
					break;
				case 'g':
					nextOperationMode = Operation_Mode_DEBUG;
					break;
				default:
					throw string("\nWrong option selected! (see readme.txt)\n");
			}
		}
		inputAvailable = 1;
}


//UserInteraction::UserInteraction(int argc, char *argv[]) :
//		argc(argc), argv(argv) {
//
//	datFilePath = NULL;
//	nextProgrammerAction = DP_NO_ACTION_FOUND;
//	nextDataLinkerRepetitionMode = Data_Linker_Repetition_Mode_NO_REPETITIONS;
//	dataLinkInputDataFormat = Data_Format_NOT_DEFINED;
//	dataLinkOutputDataFormat = Data_Format_NOT_DEFINED;
//
//	pthread_mutex_init(&coutSyncMutex, NULL);
//
//	if (argc < 2) {
//		throw string("not enough input arguments (see readme.txt)");
//	}
//
//	serialPortName = argv[1];
//
//	uint32_t conversionHelper;
//
//	istringstream(argv[2]) >> conversionHelper;
//	nextOperationMode = (operationMode) conversionHelper;
//
//	switch (nextOperationMode) {
//	case Operation_Mode_PERFORM_PROGRAMMER_ACTION: {
//		if (argc < 5) {
//			throw string("not enough input arguments (see readme.txt)");
//		}
//
//		istringstream(argv[3]) >> conversionHelper;
//		nextProgrammerAction = (programmerAction) conversionHelper;
//
//		datFilePath = argv[4];
//
//		break;
//	}
//	case Operation_Mode_USE_DATA_LINKER_SPI: {
//		if (argc < 5) {
//			throw string("not enough input arguments (see readme.txt)");
//		}
//
//		istringstream(argv[3]) >> conversionHelper;
//		nextDataLinkerRepetitionMode = (dataLinkerRepetitionMode) conversionHelper;
//
//		istringstream(argv[4]) >> conversionHelper;
//		dataLinkInputDataFormat = (dataFormat) conversionHelper;
//
//		if (nextDataLinkerRepetitionMode != Data_Linker_Repetition_Mode_REPETITIVE_WITH_BYTE_COUNT) {
//			if (argc < 6) {
//				throw string("not enough input arguments (see readme.txt)");
//			}
//
//			istringstream(argv[5]) >> conversionHelper;
//			nextDataLinkMode = (dataLinkMode) conversionHelper;
//		}
//
//		if (nextDataLinkMode != Data_Link_Mode_SEND || nextDataLinkerRepetitionMode == Data_Linker_Repetition_Mode_REPETITIVE_WITH_BYTE_COUNT) {
//			if (argc < 7) {
//				throw string("not enough input arguments (see readme.txt)");
//			}
//			istringstream(argv[6]) >> conversionHelper;
//			dataLinkOutputDataFormat = (dataFormat) conversionHelper;
//		}
//
//		break;
//	}
//	case Operation_Mode_USE_DATA_LINKER_UART: {
//		if (argc < 5) {
//			throw string("not enough input arguments (see readme.txt)");
//		}
//
//		istringstream(argv[3]) >> conversionHelper;
//		dataLinkInputDataFormat = (dataFormat) conversionHelper;
//
//		istringstream(argv[4]) >> conversionHelper;
//		dataLinkOutputDataFormat = (dataFormat) conversionHelper;
//
//		break;
//	}
//	default:
//		throw string("no such operation mode available (see readme.txt)");
//	}
//
//	inputAvailable = 1;
//}

dataLinkMode UserInteraction::getSendData(vector<uint8_t>& sendData) {
	sendData.clear();
	switch (nextDataLinkerRepetitionMode) {
	case Data_Linker_Repetition_Mode_ONE_SHOT: {
		switch (dataLinkInputDataFormat) {
		case Data_Format_BIN: {
			std::istream_iterator<char> it(cin);
			std::istream_iterator<char> end;
			sendData.assign(it, end);
			break;
		}
		case Data_Format_DEC: {
			cin >> dec;
			std::istream_iterator<uint32_t> it(cin);
			std::istream_iterator<uint32_t> end;
			sendData.assign(it, end);
			break;
		}
		case Data_Format_HEX: {
			cin >> hex;
			std::istream_iterator<uint32_t> it(cin);
			std::istream_iterator<uint32_t> end;
			sendData.assign(it, end);
			break;
		}
		default:
			throw string("Wrong data linker format (see readme.txt)");

		}
		inputAvailable = 0;
		break;
	}
	case Data_Linker_Repetition_Mode_REPETITIVE_WITH_BYTE_COUNT: {
		uint32_t data, conversionHelper;
		uint32_t byteCount;

		cin >> dec >> conversionHelper;
		nextDataLinkMode = (dataLinkMode) conversionHelper;
		if (nextDataLinkMode != Data_Link_Mode_RECEIVE && nextDataLinkMode != Data_Link_Mode_SEND && nextDataLinkMode != Data_Link_Mode_SEND_AND_RECEIVE) {
			throw string("Wrong data link mode selected (see readme.txt)");
		}

		cin >> dec >> byteCount;
		if (byteCount == 0) {
			inputAvailable = 0;
			nextDataLinkMode = Data_Link_Mode_SEND;
			break;
		}

		sendData.reserve(byteCount);

		if (nextDataLinkMode != Data_Link_Mode_RECEIVE) {
			switch (dataLinkInputDataFormat) {
			case Data_Format_BIN: {
				break;
			}
			case Data_Format_DEC: {
				cin >> dec;
				break;
			}
			case Data_Format_HEX: {
				cin >> hex;
				break;
			}
			default:
				throw string("Wrong data linker format (see readme.txt)");
			}

			for (uint32_t i = 0; i < byteCount; i++) {
				cin >> data;
				sendData.push_back(data);
			}
		} else {
			sendData.resize(byteCount);
		}

		break;
	}
	case Data_Linker_Repetition_Mode_CONTINUOUS: {
		throw string("Repetition with timeout not implemented here, program should not reach this!");
		break;
	}
	default:
		throw string("Wrong repetition mode (see readme.txt)");
	}

	return nextDataLinkMode;
}

void UserInteraction::outputData(vector<uint8_t>& outputData) {
	pthread_mutex_lock(&coutSyncMutex);
	for (uint32_t i = 0; i < outputData.size(); i++) {
		switch (dataLinkOutputDataFormat) {
		case Data_Format_BIN: {
			cout << (char) outputData[i];
			break;
		}
		case Data_Format_DEC: {
			cout << dec << (uint32_t) outputData[i] << " ";
			break;
		}
		case Data_Format_HEX: {
			cout << hex << (uint32_t) outputData[i] << " ";
			break;
		}
		default:
			throw string("Wrong data linker format (see readme.txt)");
		}
	}
	cout.flush();
	pthread_mutex_unlock(&coutSyncMutex);
}
