#!/bin/bash
if [ "$(uname)" == "Darwin" ]; then
    clang++ -pthread -o fpga_programmer -O3 -Wall -I header/ source/*.cpp source/serial/*.cpp source/serial/impl/unix.cpp
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    g++ -pthread -o fpga_programmer -O3 -Wall -I header/ source/*.cpp source/serial/*.cpp source/serial/impl/unix.cpp -lrt
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
	echo "This is untested, but might work"
    g++ -pthread -o fpga_programmer -O3 -Wall -I header/ source/*.cpp source/serial/*.cpp source/serial/impl/unix.cpp -lrt
fi

