/*
 * compression.h
 *
 *  Created on: 01.11.2013
 *      Author: Yaro
 */

#ifndef COMPRESSION_H_
#define COMPRESSION_H_

#include <vector>
#include "programmerEmulation.h"

void compress(std::vector<uint8_t>& compressedFPGAProgrammingSequence, std::vector<uint16_t>& rowDataCount, std::vector<uint8_t>& FPGAProgrammingSequence);
void decompressDebug(std::vector<uint8_t>& compressedFPGAProgrammingSequence, std::vector<uint8_t>& FPGAProgrammingSequence);


#endif /* COMPRESSION_H_ */
