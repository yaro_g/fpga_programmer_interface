/*
 * programmerInteractions.h
 *
 *  Created on: 04.11.2013
 *      Author: Yaro
 */

#ifndef PROGRAMMERINTERACTIONS_H_
#define PROGRAMMERINTERACTIONS_H_

#include "serial/serial.h"
#include <inttypes.h>
#include <iostream>
#include <string>
#include <vector>
#include "userInputFormats.h"
#include "programmerEmulation.h"

typedef enum programmerCommand {
	Programmer_Command_WAIT_FOR_COMMAND, Programmer_Command_SEND_DATA, Programmer_Command_PRINT_STRING, Programmer_Command_PRINT_STD_MESSAGE,

	Programmer_Command_GET_ROW_SIGNALS, Programmer_Command_GET_ROW_SIGNALS_COMPRESSED,

	Programmer_Command_FINISH_CURRENT_ACTION
} programmerCommand;



void handle_PERFORM_ACTION_command(serial::Serial& my_serial, programmerAction nextProgrammerAction, uint8_t* fpgaImage,
		std::vector<uint8_t>& FPGAProgrammingSequence, std::vector<uint8_t>& compressedFPGAProgrammingSequence, std::vector<uint16_t>& rowDataCount);

void printStandardProgrammerMessage(uint8_t messageNumber);

uint32_t readUint32(serial::Serial& my_serial);
uint16_t readUint16(serial::Serial& my_serial);
void writeUint32(serial::Serial& my_serial, uint32_t data);
void writeUint16(serial::Serial& my_serial, uint16_t data);

#endif /* PROGRAMMERINTERACTIONS_H_ */
