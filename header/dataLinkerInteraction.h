/*
 * dataLinker.h
 *
 *  Created on: 18.11.2013
 *      Author: Yaro
 */

#ifndef DATALINKER_H_
#define DATALINKER_H_

#include "serial/serial.h"
#include <inttypes.h>
#include <iostream>
#include <vector>
#include <iterator>
#include "userInputFormats.h"
#include "UserInteraction.h"

#include "programmerEmulation.h"
#include "compression.h"
#include "programmerInteractions.h"
#include "workers.h"
#include "ThreadSafeQueue.h"

#define DATA_LINKER_PACKET_SIZE 10000

void writeDataLinkModeAndByteCount(serial::Serial& my_serial, dataLinkMode nextDataLinkMode, uint32_t byteCount);

void handle_USE_DATA_LINKER_SPI_command(serial::Serial& my_serial, UserInteraction& user);
void handle_USE_DATA_LINKER_UART_command(serial::Serial& my_serial, UserInteraction& user);

void sendAndPrintReceivedVectorSPI(serial::Serial& my_serial, vector<uint8_t>& sendData, dataLinkMode nextDataLinkMode, UserInteraction& outputUser);
void sendVectorUART(serial::Serial& my_serial, vector<uint8_t>& sendData);

#endif /* DATALINKER_H_ */
