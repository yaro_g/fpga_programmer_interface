/*
 * versionInteraction.h
 *
 *  Created on: 03.05.2014
 *      Author: Yaro
 */

#ifndef VERSIONINTERACTION_H_
#define VERSIONINTERACTION_H_

#include "serial/serial.h"
#include <inttypes.h>
#include "userInputFormats.h"
#include "UserInteraction.h"

void handle_INFO_command(serial::Serial& my_serial);


#endif /* VERSIONINTERACTION_H_ */
