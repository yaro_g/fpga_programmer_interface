/*
 * analysis.h
 *
 *  Created on: 01.11.2013
 *      Author: Yaro
 */

#ifndef ANALYSIS_H_
#define ANALYSIS_H_

#include <algorithm>
#include <set>
#include <inttypes.h>
#include <iostream>
#include <vector>


void analyseData(std::vector<uint8_t> data);



#endif /* ANALYSIS_H_ */
