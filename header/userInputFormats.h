/*
 * userInputFormats.h
 *
 *  Created on: 23.11.2013
 *      Author: Yaro
 */

#ifndef USERINPUTFORMATS_H_
#define USERINPUTFORMATS_H_

typedef enum programmerMode {
	Programmer_Mode_FPGA_PROGRAMMER = 0, Programmer_Mode_DATA_LINKER_SPI = 1, Programmer_Mode_DATA_LINKER_UART = 2, Programmer_Mode_INFO = 3
} programmerMode;

typedef enum operationMode {
	Operation_Mode_NO_COMMAND = 0,

	Operation_Mode_PERFORM_PROGRAMMER_ACTION = 1,

	Operation_Mode_USE_DATA_LINKER_SPI = 2,

	Operation_Mode_USE_DATA_LINKER_UART = 3,

	Operation_Mode_DEBUG = 4,

	Operation_Mode_INFO = 5,

	Operation_Mode_FINISH
} userCommand;

typedef enum dataFormat {
	Data_Format_NOT_DEFINED = 0,

	Data_Format_DEC = 1,

	Data_Format_HEX = 2,

	Data_Format_BIN = 3
} dataFormat;

typedef enum dataLinkMode {
	Data_Link_Mode_SEND = 0, Data_Link_Mode_SEND_AND_RECEIVE = 1, Data_Link_Mode_RECEIVE = 2
} dataLinkMode;

typedef enum dataLinkerRepetitionMode {
	Data_Linker_Repetition_Mode_NO_REPETITIONS = 0,

	Data_Linker_Repetition_Mode_ONE_SHOT = 1,

	Data_Linker_Repetition_Mode_REPETITIVE_WITH_BYTE_COUNT = 2,

	Data_Linker_Repetition_Mode_CONTINUOUS = 3
} dataLinkerkRepetitionMode;

#endif /* USERINPUTFORMATS_H_ */
