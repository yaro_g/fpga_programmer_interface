/*
 * user.h
 *
 *  Created on: 12.10.2013
 *      Author: Yaro
 */

#ifndef USER_INTERACTION_H_
#define USER_INTERACTION_H_


#include <inttypes.h>
#include <iostream>
#include <string>
#include <iterator>
#include <vector>
#include <pthread.h>
#include <getopt.h>
#include "programmerEmulation.h"
#include "programmerInteractions.h"
#include "userInputFormats.h"


class UserInteraction {
	char *serialPortName;
	operationMode nextOperationMode;

	programmerAction nextProgrammerAction;
	char *datFilePath;

	dataLinkerRepetitionMode nextDataLinkerRepetitionMode;
	dataFormat dataLinkInputDataFormat;
	dataLinkMode nextDataLinkMode;
	dataFormat dataLinkOutputDataFormat;

	int inputAvailable;

	pthread_mutex_t coutSyncMutex;

public:
	UserInteraction(int argc, char *argv[]);

	dataLinkMode getSendData(std::vector<uint8_t>& sendData);

	void outputData(std::vector<uint8_t>& outputData);

	const char* getSerialPortName() const {
		return serialPortName;
	}

	operationMode getNextOperationMode() const {
		return nextOperationMode;
	}

	programmerAction getNextProgrammerAction() const {
		return nextProgrammerAction;
	}

	char* getDatFilePath() const {
		return datFilePath;
	}

	dataLinkerRepetitionMode getNextDataLinkerkRepetitionMode() const {
		return nextDataLinkerRepetitionMode;
	}

	int isInputAvailable() const {
		return inputAvailable;
	}

	dataLinkMode getNextDataLinkMode() const {
		return nextDataLinkMode;
	}

	dataFormat getDataLinkInputDataFormat() const {
		return dataLinkInputDataFormat;
	}
};

#endif /* USER_INTERACTION_H_ */
