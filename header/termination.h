/*
 * termination.h
 *
 *  Created on: 22.12.2013
 *      Author: Yaro
 */

#ifndef TERMINATION_H_
#define TERMINATION_H_

#include <csignal>
#include <iostream>
#include <stdlib.h>
#include "UserInteraction.h"

extern unsigned char readme_txt[];
extern unsigned int readme_txt_len;

extern volatile bool terminateFlag;


void initSigtermHandler(UserInteraction& user);
void sigtermHandler(int signum);

#endif /* TERMINATION_H_ */
