/*
 * wqueue.h
 *
 *  Created on: 22.11.2013
 *      Author: Yaro
 */

#ifndef THREADSAFEQUEUE_H_
#define THREADSAFEQUEUE_H_

#include <pthread.h>
#include <list>
#include "userInputFormats.h"

using namespace std;

template<typename T> class ThreadSafeQueue {
	uint8_t eofReached;
	list<T> m_queue;
	pthread_mutex_t m_mutex;
	pthread_cond_t m_condv;

public:
	ThreadSafeQueue() {
		pthread_mutex_init(&m_mutex, NULL);
		pthread_cond_init(&m_condv, NULL);
		eofReached = 0;
	}

	~ThreadSafeQueue() {
		pthread_mutex_destroy(&m_mutex);
		pthread_cond_destroy(&m_condv);
	}

	void add(T item) {
		pthread_mutex_lock(&m_mutex);
		m_queue.push_back(item);
		pthread_cond_signal(&m_condv);
		pthread_mutex_unlock(&m_mutex);
	}

	T remove() {
		pthread_mutex_lock(&m_mutex);
		while (m_queue.size() == 0) {
			pthread_cond_wait(&m_condv, &m_mutex);
		}
		T item = m_queue.front();
		m_queue.pop_front();
		pthread_mutex_unlock(&m_mutex);
		return item;
	}

	size_t size() {
		pthread_mutex_lock(&m_mutex);
		size_t size = m_queue.size();
		pthread_mutex_unlock(&m_mutex);
		return size;
	}

	void waitForNontrivialQueue() {
		pthread_mutex_lock(&m_mutex);
		while (m_queue.size() == 0 && !eofReached) {
			pthread_cond_wait(&m_condv, &m_mutex);
		}
		pthread_mutex_unlock(&m_mutex);
	}

	uint8_t isEofReached() const {
		return eofReached;
	}

	void setEofReached(uint8_t eofReached) {
		pthread_mutex_lock(&m_mutex);
		this->eofReached = eofReached;
		pthread_cond_signal(&m_condv);
		pthread_mutex_unlock(&m_mutex);
	}
};
#endif /* THREADSAFEQUEUE_H_ */
