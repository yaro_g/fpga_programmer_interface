Not all action codes are currently implemented in main program, but can easily be.


Action codes:										Compatible with hardware 
#define DP_DEVICE_INFO_ACTION_CODE              1   yes
#define DP_READ_IDCODE_ACTION_CODE              2   yes
#define DP_ERASE_ACTION_CODE                    3	yes
#define DP_PROGRAM_WITHOUT_VERIFY_ACTION_CODE   4	yes  
#define DP_PROGRAM_ACTION_CODE                  5	yes
#define DP_VERIFY_ACTION_CODE                   6	yes
#define DP_ENC_DATA_AUTHENTICATION_ACTION_CODE  7	no
#define DP_ERASE_ARRAY_ACTION_CODE              8	yes
#define DP_PROGRAM_ARRAY_ACTION_CODE            9	yes
#define DP_VERIFY_ARRAY_ACTION_CODE             10	yes
#define DP_ERASE_FROM_ACTION_CODE               11	maybe with appropriate .dat File
#define DP_PROGRAM_FROM_ACTION_CODE             12	maybe with appropriate .dat File
#define DP_VERIFY_FROM_ACTION_CODE              13	maybe with appropriate .dat File
#define DP_ERASE_SECURITY_ACTION_CODE           14	no
#define DP_PROGRAM_SECURITY_ACTION_CODE         15	no
#define DP_PROGRAM_NVM_ACTION_CODE              16	no
#define DP_VERIFY_NVM_ACTION_CODE               17	no
#define DP_VERIFY_DEVICE_INFO_CODE              18	yes
#define DP_READ_USERCODE_ACTION_CODE            19	yes
#define DP_PROGRAM_NVM_ACTIVE_ARRAY_CODE        20	no
#define DP_VERIFY_NVM_ACTIVE_ARRAY_CODE         21	no
#define DP_IS_CORE_CONFIGURED_ACTION_CODE       22	yes


